package main

import (
	"encoding/json"
	"time"

	"bitbucket.org/uwaploe/pducom"
	"github.com/gomodule/redigo/redis"
)

type rawMessage struct {
	T    time.Time     `json:"time"`
	Src  string        `json:"source"`
	Data pducom.RawAdc `json:"data"`
}

type procMessage struct {
	T    time.Time `json:"time"`
	Src  string    `json:"source"`
	Data [8]Metric `json:"data"`
}

func dataConverter(conn redis.Conn, sens map[int]Sensor,
	ch <-chan rawMessage, name string) {
	for rec := range ch {
		pm := procMessage{T: rec.T, Src: rec.Src}
		for i, val := range rec.Data.Data {
			if s, ok := sens[i]; ok {
				x := s.Convert(val)
				pm.Data[i] = x
			}
		}
		b, _ := json.Marshal(pm)
		conn.Do("PUBLISH", "proc."+name+"."+rec.Src, b)
	}
}
