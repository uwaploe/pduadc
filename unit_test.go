package main

import (
	"fmt"
	"testing"
)

func TestConvert(t *testing.T) {
	table := []struct {
		s   Sensor
		in  uint16
		out string
	}{
		{
			s:   Sensor{Coeff: []float64{0, 0.008741259, 0}},
			in:  33619,
			out: "293.872",
		},
	}

	for _, e := range table {
		m := e.s.Convert(e.in)
		out := fmt.Sprintf("%.3f", m.Val)
		if out != e.out {
			t.Errorf("Bad value; expected %s, got %s", e.out, out)
		}
	}
}
