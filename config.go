package main

import (
	"fmt"
	"io/ioutil"

	"github.com/BurntSushi/toml"
)

type Metric struct {
	Name  string  `json:"name"`
	Val   float32 `json:"val"`
	Units string  `json:"units"`
}

type Sensor struct {
	Name    string    `toml:"name"`
	Message string    `toml:"msg"`
	Channel int       `toml:"chan"`
	Coeff   []float64 `toml:"coeff"`
	Units   string    `toml:"units"`
}

func (s Sensor) Convert(adval uint16) Metric {
	var x, y float64

	x = float64(adval)
	n := len(s.Coeff)
	for i := n - 1; i > 0; i-- {
		y = (s.Coeff[i] + y) * x
	}
	y += s.Coeff[0]
	return Metric{Val: float32(y), Units: s.Units, Name: s.Name}
}

type sysConfig struct {
	Sens []Sensor `toml:"sensor"`
}

func loadConfiguration(cfgfile string) (map[string]map[int]Sensor, error) {
	m := make(map[string]map[int]Sensor)

	b, err := ioutil.ReadFile(cfgfile)
	if err != nil {
		return m, fmt.Errorf("Reading config file: %w", err)
	}

	var cfg sysConfig
	if err = toml.Unmarshal(b, &cfg); err != nil {
		return m, fmt.Errorf("Parsing config file: %w", err)
	}

	for _, s := range cfg.Sens {
		entry, ok := m[s.Message]
		if !ok {
			entry = make(map[int]Sensor)
			m[s.Message] = entry
		}
		entry[s.Channel] = s
	}

	return m, nil
}
