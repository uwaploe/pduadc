// Pduadc subscribes to the raw PDU/PFU ADC messages, processes them and publishes
// the processed data.
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"github.com/gomodule/redigo/redis"
)

const Usage = `Usage: pduadc [options] cfgfile [name]

Subscribe to the raw ADC messages from the PDU or PFU, process them according
to the configuration file and publish the processed data. Name is either "pdu"
or "pfu" (default is "pdu")
`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	rdAddr   string = "localhost:6379"
	msgQueue int    = 10
)

// Pass all received Redis pub-sub messages to a Go channel.
func pubsubReader(psc redis.PubSubConn, qlen int) <-chan redis.Message {
	c := make(chan redis.Message, qlen)
	go func() {
		defer close(c)
		for {
			switch msg := psc.Receive().(type) {
			case error:
				log.Println(msg)
				return
			case redis.Subscription:
				if msg.Count == 0 {
					log.Println("Pubsub channel closed")
					return
				} else {
					log.Printf("Subscribed to %q", msg.Channel)
				}
			case redis.Message:
				select {
				case c <- msg:
				default:
					log.Printf("Queue full, %q message dropped", msg.Data)
				}
			}
		}
	}()

	return c
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.IntVar(&msgQueue, "qlen",
		lookupEnvOrInt("ADC_QLEN", msgQueue),
		"message queue length")
	flag.StringVar(&rdAddr, "rdaddr",
		lookupEnvOrString("PDU_REDIS", rdAddr),
		"Redis host:port for message publishing")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func newPool(server string) *redis.Pool {
	return &redis.Pool{
		MaxIdle:     3,
		MaxActive:   16,
		IdleTimeout: 120 * time.Second,
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", server)
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			if time.Since(t) < time.Minute {
				return nil
			}
			_, err := c.Do("PING")
			return err
		},
	}
}

func main() {
	args := parseCmdLine()

	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	devName := "pdu"
	if len(args) > 1 {
		devName = args[1]
	}

	// Drop log timestamps when running under Systemd
	if _, ok := os.LookupEnv("JOURNAL_STREAM"); ok {
		log.SetFlags(0)
	}

	// Redis connection for the pub-sub subscription
	rdconn, err := redis.Dial("tcp", rdAddr)
	if err != nil {
		log.Fatalf("Cannot connect to Redis: %v", err)
	}
	defer rdconn.Close()
	psc := redis.PubSubConn{Conn: rdconn}

	// Redis connection pool for publishers
	pool := newPool(rdAddr)

	cfg, err := loadConfiguration(args[0])
	if err != nil {
		log.Fatal(err)
	}

	// Create data handlers
	chmap := make(map[string]chan rawMessage)
	for src, cvt := range cfg {
		ch := make(chan rawMessage, 4)
		go dataConverter(pool.Get(), cvt, ch, devName)
		chmap["data."+devName+"."+src] = ch
		defer close(ch)
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			psc.PUnsubscribe()
		}
	}()

	ch := pubsubReader(psc, msgQueue)
	psc.PSubscribe("data." + devName + ".ADC*")
	if devName == "pdu" {
		psc.Subscribe("data." + devName + ".HCB.ADC")
	}

	log.Printf("inVADER PDU/PFU ADC processor (%s)", Version)

	for msg := range ch {
		rec := rawMessage{}
		err = json.Unmarshal(msg.Data, &rec)
		if err != nil {
			log.Printf("Invalid JSON: %q", msg.Data)
			continue
		}
		if cvtch, ok := chmap[msg.Channel]; ok {
			cvtch <- rec
		}
	}

}
