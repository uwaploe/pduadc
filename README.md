# Process PDU/PFU ADC data

Pduadc subscribes to the Redis pub-sub channels providing the raw ADC data from the inVADER PDU or PFU boards and publishes new messages with the processed values.

## Installation

Download the most recent Debian package from the Downloads section of this repository to the inVADER SIC and install using `dpkg`.

``` shellsession
$ dpkg -i pduadc_$VERSION_amd64.deb
```

The package provides two Systemd services, `pduadc.service` to process the PDU data and `pfuadc.service` to process the PFU data. The [TOML format](https://toml.io/en/) coefficient files are `/usr/local/etc/adc/adc-pdu.toml` and `/usr/local/etc/adc/adc-pfu.toml`.

## Configuration

Here is an example of a coefficient file entry:

``` toml
[[sensor]]
name = "SYS PDUA 12V Current"
msg = "ADC1R"
chan = 0
coeff = [0.0, 0.0125, 0.0]
units = "mA"
```

*Name* is the signal name, *msg* is the NMEA message that was the source of the data, *chan* is the ADC channel, *coeff* is the array of coefficients for the conversion equation show below and *units* are the units of the output value.

``` python
value = coeff[0] + coeff[1]*counts + coeff[2]*counts**2
```
