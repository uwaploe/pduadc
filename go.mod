module bitbucket.org/uwaploe/pduadc

go 1.13

require (
	bitbucket.org/uwaploe/pducom v0.9.11
	github.com/BurntSushi/toml v0.3.1
	github.com/gomodule/redigo v2.0.0+incompatible
)
